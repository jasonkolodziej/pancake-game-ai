//
// Created by Jason Kolodziej on 10/18/18.
//

#ifndef PA3_AI_MODULE_H
#define PA3_AI_MODULE_H
#include <vector>
using namespace std;

class AI_Module{
    int diff = 0;

    // final reference that Game_Interface reviews to see who wins
    vector<int> pancake_stack;

public:
    vector<int>* get_stack(void){return &pancake_stack;}
    int* get_diff(void){return &diff;}
    AI_Module() =default;
};

#endif //PA3_AI_MODULE_H
