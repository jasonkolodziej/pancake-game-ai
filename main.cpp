#include "Game_Interface.h"

int main(){
    Game_Interface g;
    //g.test_pointers();
    initscr();		//Creates a window
    cbreak();		//Disables needing a newLine to pick up inputs
    noecho();		//Turns off echoing
    /// Cole
   g.splashScreen();	//Calls intro screen
    /// Kevin
//    g.generate_stacks(7);
//    g.draw_stacks();
    refresh();
    getch();
    endwin();
    return 0;
}