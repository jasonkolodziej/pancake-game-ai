//
// Created by Jason Kolodziej on 10/18/18.
//

#ifndef PA3_GAME_INTERFACE_H
#define PA3_GAME_INTERFACE_H
#include <ncurses.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <algorithm>
#include <string>
#include <random>
#include <sstream>
#include "User.h"
#include "AI_Module.h"
#include "find_solution.h"
using namespace std;

class Game_Interface{
    User current_user;
    AI_Module current_ai;

    // final pair of initials and score before going out to file
    pair<string*,int*> final_score_inits;

    // pairs of initials and scores already on file
    vector<pair<string,int>> top_scores;

    // descending score sort
    static bool sort_scores(const pair<string,int> &a, const pair<string,int> &b){ return (a.second > b.second); }

    //// TODO: move public declaration to a final location after merging code
public:

////////////////////////////////////////////////////////
/////// TASK 8: Game over? (stack{s} are sorted?),//////
/////// Sort and write scores out to disk //////////////
/////// Ask player if they'd like to play again ////////
////////////////////////////////////////////////////////
    int get_score_file(){
        // insert to the top_scores vector
        fstream fs;
        fs.open ("scores", fstream::in);
        string line, initials;
        int score;
        getline(fs, line);
        istringstream iss1(line);
        while((iss1 >> initials && iss1 >> score) || !iss1.eof()){
            // return if there is an error
            if(iss1.fail()) return 1;
            pair<string,int> insert(initials,score);
            top_scores.push_back(insert);
        }
        return 0;
    }

    void update_score_file(){
        top_scores.push_back(pair<string, int>(*final_score_inits.first,*final_score_inits.second));
        sort(top_scores.begin(),top_scores.end(),sort_scores);
        top_scores.erase(top_scores.end()-1);
        fstream fs;
        fs.open("scores", fstream::out | fstream::trunc);
        for(auto& pair : top_scores) fs << pair.first << " " << pair.second << " ";
    }

    int play_again(){
        char ch = ' ';
        erase();
        printw("Would you like to play again? [Y|N]\n");
        refresh();
        ch = getch();
        if(ch == 'N' || ch == 'n') return 1;
        else if(ch == 'Y' || ch == 'y') return 0;
        else play_again();
    }

    void win_checker(){
        const vector<int> * u = current_user.get_stack();
        const vector<int> * s = current_ai.get_stack();
        do{
            //// TODO: ask for input of user and ai module.
            //// TODO: update their records
        }while(!is_sorted(u->begin(),u->end()) &&
                !is_sorted(s->begin(),s->end()));
    }


////////////////////////////////
// TASK 4: Display Score File //
////////////////////////////////
void ask_for_initials(){
    char char_initials[ 3 ];
    erase();
    refresh();
    wmove(stdscr,0,0);
    printw("Input your initials : \n"); 
    getnstr(char_initials, sizeof(char_initials)-1); 
    string initials(char_initials);
    if ((initials.length() > 3) || (initials.length() < 2)) clrtoeol(); 

    *current_user.get_initials() = initials;
}

void print_score_file(){
    //display contents of the score.txt file onto the screen
    fstream fs;
    fs.open ("scores", fstream::in);
    string line, initials;
    int score;
    getline(fs, line);
    istringstream iss1(line);
    erase();
    while((iss1 >> initials && iss1 >> score) || !iss1.eof()){
        // return if there is an error
        if(iss1.fail()) return;
        printw("%s : %d\n", initials, score);
    }
    refresh();
}

///////////////////////
// TASK 3: SHUFFLING //
///////////////////////

    void get_stack_input() {
        // get user input for each pancake size
        int numPancakes = *current_user.get_n();
        vector<int> * user_stack = current_user.get_stack();
        int line = 0;
        for (int i=0; i<numPancakes; i++) {
            int nextNum = -1;
            // make sure input is an integer from 1 to numPancakes
            while (nextNum<1 || nextNum>numPancakes) {
                wmove(stdscr,line,0);
                printw("Enter %d",i+1);
                printw(((i+1)%10 == 1) ? "st" : ((i+1)%10 == 2) ? "nd" : ((i+1)%10 == 3) ? "rd" : "th");
                printw(" number (1-%d): ",numPancakes);
                char next = getch();
                nextNum = next-'0';
                if (nextNum<1 || nextNum>numPancakes) {
                    line++;
                    wmove(stdscr,line,0);
                    printw("Invalid input: %c",next);
                    line-=2;
                }
                line++;
            }
            user_stack->push_back(nextNum);
        }
    }

    void generate_stacks() {
        wmove(stdscr,0,0);
        char s = 'z';
        while (s != 's' && s != 'r') {
            printw("Enter S to specify initial state or R to randomize order: ");
            s = tolower(getch());
            if (s != 's' && s != 'r') {
                clear();
                wmove(stdscr,0,0);
                printw("Invalid input: %c",s);
                wmove(stdscr,1,0);
            }
        }
        clear();
        current_user.generate_stack();
        vector<int> * stack1 = current_user.get_stack();
        if (s == 's')
            get_stack_input();
        else {
            wmove(stdscr,0,0);
            unsigned seed = chrono::system_clock::now().time_since_epoch().count();
            shuffle (stack1->begin(), stack1->end(), default_random_engine(seed));
        }
        vector<int>* stack2 = current_ai.get_stack();
        stack2->operator=(*stack1);
    }

/// debuging function for generate stacks
//    void test_pointers(){
//        current_user.generate_stack();
//        vector<int> * stack1 = current_user.get_stack();
//        unsigned seed = chrono::system_clock::now().time_since_epoch().count();
//        shuffle (stack1->begin(), stack1->end(), default_random_engine(seed));
//        vector<int>* stack2 = current_ai.get_stack();
//        stack2->operator=(*stack1);
//        const vector<int> humanStack = *current_user.get_stack();
//        const vector <int> aiStack = *current_ai.get_stack();
//        int maxWidth = *max_element(humanStack.begin(),humanStack.end());
//    }
/////////////////////
// TASK 5: DRAWING //
/////////////////////

    void draw_pancake(int width, int maxWidth, int x, int y) {
        // draw a pancake with input top-left coordinates and input width and max stack width
        wmove(stdscr,y,x+maxWidth-width);
        waddch(stdscr,'+');
        for (int i=0; i<2*width-1; i++)
            waddch(stdscr,'-');
        waddch(stdscr,'+');
        wmove(stdscr,y+1,x+maxWidth-width);
        waddch(stdscr,'|');
        wmove(stdscr,y+1,x+maxWidth);
        printw("%d",width);
        wmove(stdscr,y+1,x+maxWidth+width);
        waddch(stdscr,'|');
        wmove(stdscr,y+2,x+maxWidth-width);
        waddch(stdscr,'+');
        for (int i=0; i<2*width-1; i++)
            waddch(stdscr,'-');
        waddch(stdscr,'+');
    }

    void draw_stack(const vector<int> stack, int x, int y, string label) {
        int maxWidth = *max_element(stack.begin(),stack.end());
        wmove(stdscr,y,x+maxWidth-label.length()/2);
        printw(label.c_str());
        for (int i=0; i<stack.size(); i++)
            draw_pancake(stack.at(i),maxWidth,x,y+3*i+1);
    }

    void draw_stacks() {
        printw("draw");
        const vector<int> humanStack = *current_user.get_stack();
        const vector <int> aiStack = *current_ai.get_stack();
        clear();
        int maxWidth = *max_element(humanStack.begin(),humanStack.end());
        draw_stack(humanStack,5,2,"HUMAN");
        draw_stack(aiStack,maxWidth*2+20,2,"AI");
    }
////////////////////////////////////////////////////////
/////// TASK 2: Ask User for 'n' (# of pancakes 2-9),///
/////// then diff, (1-n), strength for AI //////////////
////////////////////////////////////////////////////////
    void ask_for_n_diff(){
        int n, diff;
        n = diff = 0;
        while (n<2 || n>9) {
            erase();
            refresh();
            wmove(stdscr,0,0);
            printw("Specify the number of pancakes (2-9): \n");
            char next = getch();
            n = next-'0';
            if (n<2 || n>9) clrtoeol();
        }
        *current_user.get_n() = n;
        while (diff<1 || diff>n) {
            erase();
            refresh();
            wmove(stdscr,0,0);
            printw("Specify the level of difficulty (1-%d): \n",n);
            char d = getch();
            diff = d -'0';
            if (diff<1 || diff>n) clrtoeol();
        }
        *current_ai.get_diff() = diff;
        // debugging
        printw("Current user has %d & Current diff for AI is %d\n",*current_user.get_n(), *current_ai.get_diff());
    }
//////////////////////////////////////
    int game()
    {
        printw("Starting Game\n");
        ask_for_n_diff();
        //// TODO : Show leader board.
        generate_stacks();
        draw_stacks();
        play_again();
        printw("Ending Game\n");
        refresh();
        return 0;
    }

    int tutorial()
    {
        char ch = ' ';
        erase();
        printw("Would you like to view a tutorial? [Y|N]\n");
        refresh();
        ch = getch();
        if(ch == 'N' || ch == 'n')
        {
            game();
            return 1;
        }
        else if(ch == 'Y' || ch == 'y')
        {
            printw("Starting Tutorial\n");
            printw("Ending Tutorial\n");
            refresh();
            game();
            return 0;
        }
        else
        {
            return tutorial();
        }
    }

    int splashScreen()
    {
        char ch = ' ';
        //Prints out game and team info
        attron(A_BOLD);
        printw("The Game Of Pancake Sorting\n");
        printw("Brought to you by\nTeam 23: The TBD's\n");
        attroff(A_BOLD);
        printw("Kevin Sittser\nNabin Gautam\nJason Kolodziej\nVishaal Makani\nCole Waldron\n");
        attron(A_BLINK);
        printw("Press ENTER to start\n");
        attroff(A_BLINK);
        while(true)	//Waits for user to enter ENTER, then calls tutorial
        {
            refresh();
            ch = getch();
            if(ch == '\n')
            {
                tutorial();
                break;
            }
        }
        refresh();
        //Waits for any input
        printw("Press any key to exit\n");
        getch();
        endwin();	//Closes window
        return 0;
    }

    Game_Interface() {
       // User u("JAK",6);
        //current_user = u;
        final_score_inits.first = current_user.get_initials();
        final_score_inits.second = current_user.get_n();

    }

    ~Game_Interface(){
        print_score_file();
    };



    };

#endif //PA3_GAME_INTERFACE_H