## 1. Purpose  
**What it does:**

The purpose of this project is to implement a simple Artificial Intelligence (AI) module that will be 
able to play against human user interaction towards a game called ________. 

**1.1) Problem:**

The problem is coming up with a simple interface that allows any human to interact with the game system.
Providing a simple interaction schema and an efficient data structuring will allow this program to run with
little to none resource usage, attract users of all ages, and be compiled on any UNIX based system.

**1.2) Need:**

This game is not necessarily needed, however, the design of this game is a bit different than games currently
designed for most UNIX based systems. Therefore, the team wanted to bring a new side of gaming to the table
that provides eye pleasing graphics at a minimal level, a small foot print (in terms of resource usage), and
allow users to test their observation and tactic skills.
 

**1.3) Users:**

 All gamers or those interested in seeing AI implemented in a small game that will flip your stack
 upside down and inside out. Any age group would be able to play this simple game where there is not
 much interaction other than typing in your initials or using the arrow keys to decipher which pancake
 to flip. Or clicking the `Get Help` button on during the game session to request help on how to beat
 the computer.
  
## 2. High Level Designs  
**Purpose:**
In the [High Level Diagram](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#high-level-diagram),
`Game_Interface` class handles interaction from the user and calls on classes `User`,`Hint` and `AI_Module` to
to hold data required to run the game, as well as, provide some algorithmic functions in producing the next be the
computer. The `Game_Interface` class then handles switching back and forth between the computer and the user
playing the game. This class also will handle the score file that is created to hold the scores
of the **_Top 5_** players.

**Reasoning:** 
By implementing most of the classes `User`, `AI_Module`, `Hint`,`Game_Interface` and their code 
into header files and the rest of startup code in the main users would be able to compile
the `main.cpp` file very easily by running the `make.sh` script on their UINX terminal. Then,
the user can start the game by typing `./flapjack` on the terminal.  

Overall, seeing other implementations of games made, we wanted to reduce the hassle of having to compile
so many different files and run into errors. From our past experiences, as a team, we kept in mind that our
audience is very broad. With such a wide scope, we wanted to make sure that people are not having to spend
**too** much time compiling and more time trying to beat the smarts stored inside the game!  

**2.1) Splash Screen**  

**2.2) Pancake Count and Difficulty Level** 

_Purpose_ : Collect the pancake count and difficulty level from the user, so that the `Game_Interface`
class is able to set up the `User`, `AI_Module`, `Hint` classes. So that they can be used throughout the game.

_Justification_ : This can be useful in calculating user hints for their next move
and the computer to help calculate how they will play against the player.

_Role_ : This is essential to the game. For the `AI_Module` to play against the user correctly and be fair to the user
the team must implement this task.

**2.3) Initial Order State**  

**2.4) Top Scores**  
_Purpose_ : Task 4 of this AI-based game project deals with displaying the top 5 scores by writing to/updating a score.txt after the game is over. For this specific task, the `Game_Interface` class, and `User` class entities each have methods that allow the score to be calculated, updated in the _score.txt_ file, and displayed.

_Justification_  : The `Game_Interface` class entity is designed to have the methods that will update and print the _score.txt_ file once the game is done, and also gets the difficulty, diff, and number of pancakes, n, from the user. This is used to calculate the user score, which is done in the `User` class, and is added to the data structure of initials and score in the `Game_Interface` class. 

_Role_  : Using methods like, update_score_file() and print_score_file(), the game interface is what will allow the top 5 scores to be displayed. The score is calculated and set using methods in the `User` class, and added to the private Map of scores (initials and scores) in the `Game_Interface` class. The scores are sorted, and _scores.txt_ file is updated to show the top 5 scores at the end of the game. 

**2.5) Visualization** 
 
**2.6) Game Controls**  
_Purpose_ : Task 6 of this AI-based game project deals with prompting the user to select a pancake on the user-side using the arrow keys. After the user selects a pancake, the move will be checked before it is executed.   
_Justification_  : For this task the `Game_Interface` entity is going to be used. The `Game_Interface` class is going to prompt the user to select a pancake using the arrow keys initially, and each time the AI makes its move. The user will select the pancake to flip, and the `Game_Interface` will also check before flipping that the move is valid.  
_Role_  : Using methods like, `void flip_pancake()` and `bool valid_move()`, the  `Game_Interface` class will be in charge of the game controls. Allowing the user to make a flip, and checking if the move is valid are crucial to the game's correctness and functionality. 

**2.7) Minimax Search**    

**2.8) Game Over**  

_Purpose_ : Allow ease of access to the user to play the game again. 

_Justification_ : The team does not have to implement this, however it reduces the amount of times the user has
to open up the game if they are wanting to play more than once. Also, part of this task is important
because at this point and time the program will be calculating the top 5 scores, sort them in a descending fashion
then writing out to the disk.

_Role_ : Part of this task is optional and part is required. However, the team is keeping the end user in mind and asserting
the optional part task as required to make players continuously play the game because we hope that they will want to try and
beat the AI embedded with in the program.

**2.9) EXTRA: Minimum move count**

_Purpose_ : Calculate the minimum number of moves required to sort the given pancake stack. 

_Justification_ : This can be useful in providing user hints for their next move not just the minimum number of moves required.

_Role_ : This is more of a feature of the game and not a necessary entity in the game. It simply makes user’s life easier when playing the game if they ever get stuck.

  
## 3. Low Level Designs 

**3.1) Splash Screen**  
_Usage_ : The `Game_Interface` class creates the splash screen on initializing the program, and waits for user input before continuing. This will be a separate function call that writes all the hard coded information to the screen.   
_Model_ : See diagram : [Low Level Diagram Model](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-model)  
_Interaction_ : See diagram : [Low Level Diagram Interaction](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-interaction)   

**3.2) Pancake Count and Difficulty Level**  
_Usage_ : The `Game_Interface` class will prompt the user for number of pancakes and difficulty level after the splash screen, this data is used to create other data structures such as the pancake stacks and minimin tree (see below).  
_Model_ : See diagram : [Low Level Diagram Model](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-model)  
_Interaction_ : See diagram : [Low Level Diagram Interaction](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-interaction)      

**3.3) Initial Order State**  
_Usage_ : The ordering of the pancake stacks will be stored in two private vectors of integers, and modifying them can only be done through a public `flipPancake` function, which as arguments takes which pancake to flip, and which player's stack to flip it in. This allows us to make sure that the move is valid, and also conserve space by having the same function for both players. Optionally, the player can select the order of the stacks at time of initializing, otherwise they are random.   
_Model_ : See diagram : [Low Level Diagram Model](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-model)  
_Interaction_ : See diagram : [Low Level Diagram Interaction](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-interaction)   

**3.4) Top Scores**    
_Usage_ : The top 5 scores will be stored in a private Map of string initials and int scores, with an additional entry for the current player. These scores are read in at the beginning of each game from a file `scores.txt` which has the top 5 scores each on their own line with initials and scores separated by a space. Initials will be two or three characters long, with only alphabet characters allowed. The player score is updated after each game, and all the scores are sorted in descending order. The scores are written back out to the file after each game.  
  Displaying the score requires the users initials to be displayed along with the score, which is accomplished by a `get_initials()` method, as well as a Map that uses the initials as a key to the score. The actual user score is determined based on how the game ended, i.e. who was able to sort the stack first (ai, user, or a tie). For calculating the score both the diff (difficulty) and n (number of pancakes) are required, which are variables in the `Game_Interface` class that are going to be public so they can be accessed by the `User` class when calculating player scores. To know when the update the user score, the stacks of both the AI and the user need to be checked after each AI move (see game over task for more info of how end of game will be handled). Before the scores are shown in the score file (using a `update_score_file()` method), a `sort_scores()` method is going to be used to sort the scores in descending order to get the top 5 scores.  
_Model_ : See diagram : [Low Level Diagram Model](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-model)  
_Interaction_ : See diagram : [Low Level Diagram Interaction](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-interaction)   
  

**3.5) Visualization**  
_Usage_ :  
_Model_ : See diagram : [Low Level Diagram Model](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-model)  
_Interaction_ : See diagram : [Low Level Diagram Interaction](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-interaction)   

**3.6) Game Controls**  
_Usage_ : 
_Model_ : See diagram : [Low Level Diagram Model](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-model)  
_Interaction_ : See diagram : [Low Level Diagram Interaction](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-interaction)   

**3.7) Minimax Search**  
_Usage_ : The AI program will make its decisions based on exploring a minimin tree. This was originally meant to be a minimax tree, but as the game does not allow for player interference, a true minimax tree is not a good option for selecting the best move, as it will assume that every other move is the worst possible move. This tree is stored and manipulated in the `AI_Module` class.   
_Model_ : See diagram : [Low Level Diagram Model](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-model)  
_Interaction_ : See diagram : [Low Level Diagram Interaction](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-interaction)    

**3.8) Game Over**  
_Usage_ : The `Game_Interface` class moves back and forth between the user and AI, getting their moves and updating the pancake stacks. After each time the AI goes, it will check for sortedness and if true, calculate the player's score and close the game. It will then ask the player to play again or quit.    
_Model_ : See diagram : [Low Level Diagram Model](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-model)  
_Interaction_ : See diagram : [Low Level Diagram Interaction](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-interaction)   

**3.9) Extra Task: Minimum move count**  
_Usage_: Minimum move count feature will be accomplished by calling functions of `Hint` class which primarily returns minimum number of moves required in order to sort the stack. 

_Model_: Minimum move count function will be in a separate class called `Hint`. It will provide have a function called `minimum_moves` which returns a vector of ints which has the sequence of flips that user needs to make in order to sort the stack. For example: If a vector of ints {5,4,1,2,3} is passed, it returns {5,3} which means flip the top 5 pancakes, and then the top 3. The size of this vector is the minimum moves required to sort the stack.

_Interaction_: `Hint` class will be called by the `Game_Interface` class. Every time a user makes a move, minimum_moves is recalculated based on the new stack. Also, `Game_Interface` can utilize `Hint` class to implement a “Show hint” button which recommends user to flip the top n pancakes leading them closer to sorting the stack.

**_Model_**  Please Visit : [Low Level Diagram Model](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-model)
**_Interaction_**  Please Visit : [Low Level Diagram Interaction](https://github.tamu.edu/ksittser/PA3_315/wiki/Design-Document#low-level-diagram-interaction)  
  
## Benefits, Assumptions, Risks/Issues  
5-6 top benefits  
all risks  
all assumptions

**Benefits**
* With the way project is implemented, it is easier to make the game AI vs AI, player vs player and not just player vs AI.
*
*
*
*

**Risk/Issues**
* The `scores.txt` file might be altered by the user, therefore the team will need to seek a way of only being accessed by the program.
*
*
*
*

**Assumptions**
* The user will try and put a number higher than 9, or the user will try and put more than three characters as their initials.
*
*
*
*
-------
# High Level Diagram
-------
![High Level Diagram](https://github.tamu.edu/ksittser/PA3_315/blob/master/Diagrams/High%20Level%20Diagram.png)
-------
# Low Level Diagram Model
-------
![Low Level Diagram Model](https://github.tamu.edu/ksittser/PA3_315/blob/master/Diagrams/Low%20Level%20Model.png)
-------
# Low Level Diagram Interaction
-------
![Low Level Diagram Interaction](https://github.tamu.edu/ksittser/PA3_315/blob/master/Diagrams/Low%20Level%20Interaction.png)

