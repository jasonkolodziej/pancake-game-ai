#include <ncurses.h>
#include <string>
#include <stdlib.h>

using namespace std;

#define ROW 40
#define COL 80
class SplashScreen
{
	public:
		SplashScreen()
		{
			initscr();				//Creates a window
			cbreak();				//Disables needing a newLine to pick up inputs
			noecho();				//Turns off echoing
			keypad(stdscr, TRUE);	//Enables other inputs like arrrow keys
		}
		int splashScreen()
		{
			char ch = ' ';
			//Prints out game and team info
			attron(A_BOLD);
			mvprintw(5, (COL-sizeof("The Game Of Pancake Sorting\n"))/2, "%s", "The Game Of Pancake Sorting\n");
			mvprintw(7, (COL-sizeof("Brought to you by\n"))/2, "%s", "Brought to you by\n");
			mvprintw(8, (COL-sizeof("Team 23: The Spatula's\n"))/2, "%s", "Team 23: The Spatula's\n");
			attroff(A_BOLD);
			string names[] =
			{
				"Kevin Sittser\n",
				"Nabin Gautam\n",
				"Jason Kolodziej\n",
				"Vishaal Makani\n",
				"Cole Waldron\n"
			};
			for(int i = 0; i < 5; i++)
			{
				mvprintw(10+2*i, (COL-names[i].length())/2, "%s", names[i].c_str());
			}
			attron(A_BLINK);
			attron(A_BOLD);
			mvprintw(20, (COL-sizeof("Press ENTER to start\n"))/2, "%s", "Press ENTER to start\n");
			attroff(A_BLINK);
			attroff(A_BOLD);
			refresh();
			while(true)	//Waits for user to enter ENTER, then calls menu
			{
				ch = getch();
				if(ch == '\n')
				{
					menu();
					break;
				}
			}
			endwin(); //Closes window
			return 0;
		}
	private:
		int menu()
		{
			string items[] =
			{
				"1: Play Game\n",
				"2: View Tutorial\n",
				"3: View High Scores\n",
				"4: Exit Game\n"
			};
			int n_item = 0;
			bool exitCond = true;
			while(exitCond)
			{
				//Displays menu
				erase();
				for(int i = 0; i < 4; i++)
				{
					mvprintw(10+i*2, (COL-items[i].length())/2, "%s", items[i].c_str());
				}
				//Displays cursor
				mvprintw(10+n_item*2, (COL-items[n_item].length())/2-3, "%s", "-> ");
				refresh();
				int ch = getch();
				//Moves cursor and takes ENTER
				switch(ch)
				{
					case KEY_UP:
						n_item--;
						break;
					case KEY_DOWN:
						n_item++;
						break;
					case 10:
						//Calls other functions
						switch(n_item)
						{
							case 0:
								game();
								break;
							case 1:
								tutorial();
								break;
							case 2:
								highScores();
								break;
							case 3:
								exitCond = false;
								break;
							default:
								break;
						}
						break;
					default:
						break;
				}
				//Handles cursor wrapping
				if(n_item == -1)
				{
					n_item = 3;
				}
				else if(n_item == 4)
				{
					n_item = 0;
				}
			}
			return 0;
		}

		int highScores()
		{
			char ch = ' ';
			erase();
			string initials[] =
			{
				"AAA",
				"BBB",
				"CCC",
				"DDD",
				"EEE",
				"FFF"
			};
			string scores[] = 
			{
				"555",
				"444",
				"333",
				"222",
				"111",
				"000"
			};
			//Displays high scores
			mvprintw(6, (COL-sizeof("Top 5 High Scores\n"))/2, "%s", "Top 5 High Scores\n");
			mvprintw(10, (COL-sizeof("Initials\tScore\n")-4)/2, "%s", "Initials\tScore\n");
			for(int i = 0; i < 5; i++)
			{
				mvprintw(12+i*2, 35, "%s", (string(initials[i] + "\t" + scores[i] + "\n")).c_str());
			}
			//Displays player score
			mvprintw(24, (COL-sizeof("Your best score\n"))/2, "%s", "Your best score\n");
			mvprintw(26, (COL-initials[5].length()-scores[5].length()-4)/2, "%s", (string(initials[5] + "\t" + scores[5] + "\n")).c_str());
			mvprintw(30, (COL-sizeof("Press ENTER to return to menu\n"))/2, "%s", "Press ENTER to return to menu\n");
			refresh();
			bool exitCond = true;
			while(exitCond)	//Waits for user to enter ENTER, then exits
			{
				ch = getch();
				if(ch == '\n')
				{
					exitCond = false;
					break;
				}
			}
			return 0;
		}

		int game()
		{
			char ch = ' ';
			erase();
			//End game prompt
			mvprintw(14, (COL-sizeof("Would you like to play again? [Y|N]\n"))/2, "%s", "Would you like to play again? [Y|N]\n");
			refresh();
			bool exitCond = true;
			while(exitCond)
			{
				ch = getch();
				if(ch == 'Y' || ch == 'y')
				{
					game();
					exitCond = false;
					break;
				}
				else if (ch == 'N' || ch == 'n')
				{
					exitCond = false;
					break;
				}
			}
			return 0;
		}

		int tutorial()
		{
			erase();
			printw("Starting Tutorial\n");
			printw("Ending Tutorial\n");
			refresh();
			return 0;
		}
};

int main()
{	
	SplashScreen splashscreen;
	splashscreen.splashScreen();	//Calls intro screen
	return 0;
}