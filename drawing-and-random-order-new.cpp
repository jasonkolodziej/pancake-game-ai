#include <iostream>
#include <algorithm>
#include <array>
#include <random>
#include <chrono>
#include <vector>
#include <math.h>
#include <ctype.h>
#include <ncurses.h>
#include <string>
using namespace std;

///////////////////////
// TASK 3: SHUFFLING //
///////////////////////

vector<int> get_stack_input(int numPancakes) {
	vector<int> stack={};
	int line=0;
	for (int i=0; i<numPancakes; i++) {
		int nextNum = -1;
		bool invalid = false;
		while (nextNum<1 || nextNum>9) {
			wmove(stdscr,line,0);
			printw("Enter %d",i+1);
			printw(((i+1)%10 == 1) ? "st" : ((i+1)%10 == 2) ? "nd" : "th");
			printw(" number (1-9): ");
			char next = getch();
			nextNum = next-'0';
			if (nextNum<1 || nextNum>9) {
				if (invalid)
					line--;
				invalid = true;
				wmove(stdscr,line,0);
				printw("Invalid input: %c",next);
				clrtoeol();
			}
			line++;
		}
		stack.push_back(nextNum);
	}
	return stack;
}

void generate_stacks(int numPancakes, vector<int>& stack1, vector<int>& stack2) {
	wmove(stdscr,0,0);
	char s = 'z';
	while (s != 's' && s != 'r') {
		printw("Enter S to specify initial state or R to randomize order: ");
		s = tolower(getch());
		if (s != 's' && s != 'r') {
			clear();
			wmove(stdscr,0,0);
			printw("Invalid input: %c",s);
			wmove(stdscr,1,0);
		}
	}
	clear();
	for (int i=1; i<=numPancakes; i++)
		stack1.push_back(i);
	if (s == 's')
		stack1 = get_stack_input(numPancakes);
	else {
		unsigned seed = chrono::system_clock::now().time_since_epoch().count();
		shuffle (stack1.begin(), stack1.end(), default_random_engine(seed));
	}
	stack2 = stack1;
}


/////////////////////
// TASK 5: DRAWING //
/////////////////////

void draw_pancake(int width, int maxWidth, int x, int y) {
	// draw a pancake with input top-left coordinates and input width and max stack width
	wmove(stdscr,y,x+maxWidth-width);
	waddch(stdscr,'+');
	for (int i=0; i<2*width-1; i++)
		waddch(stdscr,'-');
	waddch(stdscr,'+');
	wmove(stdscr,y+1,x+maxWidth-width);
	waddch(stdscr,'|');
	wmove(stdscr,y+1,x+maxWidth);
	printw("%d",width);
	wmove(stdscr,y+1,x+maxWidth+width);
	waddch(stdscr,'|');
	wmove(stdscr,y+2,x+maxWidth-width);
	waddch(stdscr,'+');
	for (int i=0; i<2*width-1; i++)
		waddch(stdscr,'-');
	waddch(stdscr,'+');
}

void draw_stack(const vector<int> stack, int x, int y, string label) {
	int maxWidth = *max_element(stack.begin(),stack.end());
	wmove(stdscr,y,x+maxWidth-label.length()/2);
	printw(label.c_str());
	for (int i=0; i<stack.size(); i++)
		draw_pancake(stack.at(i),maxWidth,x,y+3*i+1);
}

void draw_stacks(const vector<int> humanStack, const vector<int> aiStack) {
	clear();
	int maxWidth = *max_element(humanStack.begin(),humanStack.end());
	draw_stack(humanStack,5,2,"HUMAN");
	draw_stack(aiStack,maxWidth*2+20,2,"AI");
}


//////////
// MAIN //
//////////

int main() {
    initscr();
	cbreak();
	vector<int> stack1;
	vector<int> stack2;
	generate_stacks(9,stack1,stack2);
	draw_stacks(stack1,stack2);
    refresh();
	getch();
    endwin();
    return 0;
}