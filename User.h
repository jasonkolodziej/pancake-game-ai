//
// Created by Jason Kolodziej on 10/18/18.
//

#ifndef PA3_USER_H
#define PA3_USER_H
#include <string>
#include <vector>
using namespace std;

class User{
    string initials = "";
    int n=0;

    // final reference that Game_Interface reviews to see who wins
    vector<int> pancake_stack;

public:
    int* get_n(void){return &n;}
//    void set_n(int n){ this->n=n;}
    string* get_initials(void){ return &initials;}
    User(string init, int n):initials(init),n(n){};
    void generate_stack(void){ for (int i = 1; i <=n; ++i) pancake_stack.push_back(i); }
    vector<int>* get_stack(void){return &pancake_stack;}
    User()= default;
};

#endif //PA3_USER_H
